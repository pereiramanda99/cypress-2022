describe('realizar compra - vestido casual', () => {
  it('fluxo principal.ID01', () => {
    //entrando no site
    cy.visit('http://automationpractice.com/index.php?id_category=9&controller=category')
    //conferindo categoria do produto
    cy.get('.cat-name').contains('Casual Dresses')
    //adicionando produto no carrinho de compras
    cy.get('.ajax_add_to_cart_button > span').click()
    //acessando nova rota do carrinho criado
    cy.server()
    cy.route('POST', '/index.php?rand=1659665413071').as('PostAddToCart')
    //prosseguir para checkout
    cy.get('.button-container > .button-medium > span').click()
    //confirmar informações do produto
    cy.get('.cart_navigation > .button > span').click()
    //realizar login
    cy.get('#email').type('pereiramanda99@gmail.com')
    cy.get('#passwd').type('123456')
    cy.get('#SubmitLogin > span').click()
    //confirmar endereço de entrega
    cy.get('.cart_navigation > .button > span').click()
    //aceitar termos
    cy.get('#cgv').click()
    //confirmar opção de transporte
    cy.get('.cart_navigation > .button > span').click()
    //selecionar cartão como forma de pagamento
    cy.get('.bankwire').click()
    //confirmar etapa de pagamento
    cy.get('#cart_navigation > .button > span').click()
    //ir para listagem de pedidos
    cy.get('.button-exclusive').click()
  })

  it('fluxo alternativo.ID02', () => {
    //entrando no site
    cy.visit('http://automationpractice.com/index.php?id_category=9&controller=category')
    //conferindo categoria do produto
    cy.get('.cat-name').contains('Casual Dresses')
    //adicionando produto no carrinho de compras
    cy.get('.ajax_add_to_cart_button > span').click()
    //acessando nova rota do carrinho criado
    cy.server()
    cy.route('POST', '/index.php?rand=1659665413071').as('PostAddToCart')
    //prosseguir para checkout
    cy.get('.button-container > .button-medium > span').click()
    //alterar quantidade do produto
    cy.get('.cart_quantity_input').type(10)
    //confirmar informações do produto
    cy.get('.cart_navigation > .button > span').click()
    //realizar login
    cy.get('#email').type('pereiramanda99@gmail.com')
    cy.get('#passwd').type('123456')
    cy.get('#SubmitLogin > span').click()
    //confirmar endereço de entrega
    cy.get('.cart_navigation > .button > span').click()
    //aceitar termos
    cy.get('#cgv').click()
    //confirmar opção de transporte
    cy.get('.cart_navigation > .button > span').click()
    //selecionar cartão como forma de pagamento
    cy.get('.bankwire').click()
    //confirmar etapa de pagamento
    cy.get('#cart_navigation > .button > span').click()
    //ir para listagem de pedidos
    cy.get('.button-exclusive').click()
  })

  it('fluxo alternativo.ID03', () => {
    //entrando no site
    cy.visit('http://automationpractice.com/index.php?id_category=9&controller=category')
    //conferindo categoria do produto
    cy.get('.cat-name').contains('Casual Dresses')
    //adicionando produto no carrinho de compras
    cy.get('.ajax_add_to_cart_button > span').click()
    //acessando nova rota do carrinho criado
    cy.server()
    cy.route('POST', '/index.php?rand=1659665413071').as('PostAddToCart')
    //continuar compras
    cy.get('.continue > span').click()
    //acessar categoria de vestidos
    cy.get('.sf-menu > :nth-child(2) > .sf-with-ul').click()
    //selecionar vestidos de verão
    cy.get('.block_content > .tree > .last > a').click()
    //escolher produto
    cy.get('.first-in-line > .product-container > .right-block > .button-container > .ajax_add_to_cart_button > span').click()
    //acessando nova rota do carrinho criado
    cy.server()
    cy.route('POST', '/index.php?rand=1659665413071').as('PostAddToCart2')
    //prosseguir para checkout
    cy.get('.button-container > .button-medium > span').click()
    //confirmar informações do produto
    cy.get('.cart_navigation > .button > span').click()
    //realizar login
    cy.get('#email').type('pereiramanda99@gmail.com')
    cy.get('#passwd').type('123456')
    cy.get('#SubmitLogin > span').click()
    //confirmar endereço de entrega
    cy.get('.cart_navigation > .button > span').click()
    //aceitar termos
    cy.get('#cgv').click()
    //confirmar opção de transporte
    cy.get('.cart_navigation > .button > span').click()
    //selecionar cartão como forma de pagamento
    cy.get('.bankwire').click()
    //confirmar etapa de pagamento
    cy.get('#cart_navigation > .button > span').click()
    //ir para listagem de pedidos
    cy.get('.button-exclusive').click()
  })
  
  it('fluxo exceção.ID04', () => {
    //entrando no site
    cy.visit('http://automationpractice.com/index.php?id_category=9&controller=category')
    //conferindo categoria do produto
    cy.get('.cat-name').contains('Casual Dresses')
    //adicionando produto no carrinho de compras
    cy.get('.ajax_add_to_cart_button > span').click()
    //acessando nova rota do carrinho criado
    cy.server()
    cy.route('POST', '/index.php?rand=1659665413071').as('PostAddToCart')
    //prosseguir para checkout
    cy.get('.button-container > .button-medium > span').click()
    //confirmar informações do produto
    cy.get('.cart_navigation > .button > span').click()
    //realizar login
    cy.get('#email').type('pereiramanda99@gmail.com')
    cy.get('#passwd').type('12345678')
    cy.get('#SubmitLogin > span').click()
    //conferir mensagem de erro
    cy.get(':nth-child(3) > p').contains('There is 1 error')
    cy.get('ol > li').contains('Authentication failed.')
  })
})